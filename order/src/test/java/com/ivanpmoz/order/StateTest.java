package com.ivanpmoz.order;

import com.ivanpmoz.order.service.helper.state.StateFactory;
import com.ivanpmoz.order.service.helper.state.util.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StateTest {
    @Test
    void test() {
        Assertions.assertTrue(StateFactory.getInstance(null).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertTrue(StateFactory.getInstance(Constants.REGOGIDO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REPARTO)));
        Assertions.assertTrue(StateFactory.getInstance(Constants.REPARTO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.INCIDENCIA)));
        Assertions.assertTrue(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(Constants.ENTREGADO)));
    }

    @Test
    void testEstadoEntregado() {
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REPARTO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.INCIDENCIA)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.ENTREGADO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(null)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(99999)));
    }

    @Test
    void testEstadoEntregadoAlmacen() {
        Assertions.assertFalse(StateFactory.getInstance(Constants.REGOGIDO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.REPARTO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.ENTREGADO).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertTrue(StateFactory.getInstance(null).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertFalse(StateFactory.getInstance(99999).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
    }

    @Test
    void testEstadoIncidencia() {
        Assertions.assertFalse(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertTrue(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REPARTO)));
        Assertions.assertTrue(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(Constants.INCIDENCIA)));
        Assertions.assertTrue(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(Constants.ENTREGADO)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(null)));
        Assertions.assertFalse(StateFactory.getInstance(Constants.INCIDENCIA).puedoIrNuevoEstado(StateFactory.getInstance(99999)));
    }

    @Test
    void testPedidoNoExistente() {
        Assertions.assertTrue(StateFactory.getInstance(null).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REGOGIDO)));
        Assertions.assertFalse(StateFactory.getInstance(null).puedoIrNuevoEstado(StateFactory.getInstance(Constants.REPARTO)));
        Assertions.assertFalse(StateFactory.getInstance(null).puedoIrNuevoEstado(StateFactory.getInstance(Constants.INCIDENCIA)));
        Assertions.assertFalse(StateFactory.getInstance(null).puedoIrNuevoEstado(StateFactory.getInstance(Constants.ENTREGADO)));
    }



}
