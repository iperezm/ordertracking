package com.ivanpmoz.order.model.entity;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "ORDER_TBL")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Order {
    @Id
    private Long id;
    @Column(name = "STATUS_ID")
//    @NotEmpty(message = "El estado no puede ser vacío")
    private Integer statusId;
    @Column(name = "CHANGE_STATUS_DATE")
//    @NotEmpty(message = "La fecha de modificacion de estado no puede ser vacío")
    private LocalDateTime changeStatusDate;
    @Column(name = "UPDATE_STATUS_DATE")
//    @NotEmpty(message = "La fecha de procesado de modificacion de estado no puede ser vacío")
    private LocalDateTime updateStatusDate;
}

