package com.ivanpmoz.order.service.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class OrderTrackingProcessDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long orderId;
    private Integer statusId;
    private LocalDateTime changeStatusDate;
    private LocalDateTime updateStatusDate;


}
