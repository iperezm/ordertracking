package com.ivanpmoz.order.service.helper.state;

public interface State {
    default boolean puedoIrNuevoEstado(State newState) {return newState.puedeAsignarse();};
    default boolean puedeAsignarse() {return true;}
}
