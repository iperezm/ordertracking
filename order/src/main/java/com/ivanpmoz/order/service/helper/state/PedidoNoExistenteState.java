package com.ivanpmoz.order.service.helper.state;

public class PedidoNoExistenteState implements State {

    @Override
    public boolean puedoIrNuevoEstado(State newState) {
        //Puede ir al estado inicial
        return newState instanceof RecogidoAlmacenState;
    }

    @Override
    public boolean puedeAsignarse() {
        //No aplica
        return false;
    }
}
