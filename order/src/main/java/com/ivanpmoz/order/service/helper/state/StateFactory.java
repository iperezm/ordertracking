package com.ivanpmoz.order.service.helper.state;


import com.ivanpmoz.order.service.helper.state.util.Constants;

public class StateFactory {

    public static State getInstance(Integer statusId) {
        if (statusId == null) {
            return new PedidoNoExistenteState();
        } else {
            switch (statusId) {
                case Constants.REGOGIDO: return new RecogidoAlmacenState();
                case Constants.REPARTO: return new EstadoIntermedioState();
                case Constants.INCIDENCIA: return new EstadoIntermedioState();
                case Constants.ENTREGADO: return new EntregadoEstate();
                default: return new EstadoDesconocidoState();
            }
        }

    }


}
