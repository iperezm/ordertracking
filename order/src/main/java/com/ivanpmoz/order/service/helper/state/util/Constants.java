package com.ivanpmoz.order.service.helper.state.util;

public class Constants {

    public static final int REGOGIDO = 1;
    public static final int REPARTO = 2;
    public static final int INCIDENCIA = 3;
    public static final int ENTREGADO = 4;
}
