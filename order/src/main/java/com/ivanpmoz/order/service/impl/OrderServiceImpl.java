package com.ivanpmoz.order.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ivanpmoz.order.model.entity.Order;
import com.ivanpmoz.order.model.repository.OrderRepository;
import com.ivanpmoz.order.service.OrderMapper;
import com.ivanpmoz.order.service.OrderService;
import com.ivanpmoz.order.service.dto.OrderTrackingProcessDto;
import com.ivanpmoz.order.service.helper.state.StateFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    private Source pipe;
    private OrderRepository orderRepository;
    private OrderMapper orderMapper;

    public OrderServiceImpl(Source pipe, OrderRepository orderRepository, OrderMapper orderMapper) {
        this.pipe = pipe;
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Transactional
    public void trackOrders(List<OrderTrackingProcessDto> orderTrackingProcessDtoList) {
        log.debug("Into the service");
        final LocalDateTime now = LocalDateTime.now();
        //Saco de la entrada los orders que tengo que procesar
        List<OrderTrackingProcessDto> compliantOrders = getCompliantOrders(orderTrackingProcessDtoList);
        //Les aplico la fecha de proceso actual y actualizo la base de datos
        compliantOrders.forEach(o -> o.setUpdateStatusDate(now));
        List<Order> orderList = orderMapper.asOrders(compliantOrders);
        orderRepository.saveAll(orderList);
        //Envio de mensaje con los pedidos actualizados (quiza haria falta otro dto especifico)
        compliantOrders.forEach(o -> pipe.output().send(MessageBuilder.withPayload(o).build()));
    }

    private List<OrderTrackingProcessDto> getCompliantOrders(List<OrderTrackingProcessDto> orderTrackingProcessDtoList) {
        //Recojo los ids de los orders que me pasan
        List<Long> allOrderIds = orderTrackingProcessDtoList.stream().map(OrderTrackingProcessDto::getOrderId).collect(Collectors.toList());
        //Los busco en BBDD y lo paso a mapa orderId-statusId
        Map<Long, Integer> orderBBDDMap = orderRepository.findAllById(allOrderIds).stream().collect(Collectors.toMap(Order::getId, order -> order.getStatusId()));
        //Filtro y me quedo con los que pueden cambiar al estado propuesto teniendo en cuenta su estado anterior
        List<OrderTrackingProcessDto> orderTrackingProcessDtoFilteredList = orderTrackingProcessDtoList.stream().filter(dto ->
            StateFactory.getInstance(orderBBDDMap.get(dto.getOrderId())).puedoIrNuevoEstado(StateFactory.getInstance(dto.getStatusId()))).collect(Collectors.toList());
        log.debug("*************************************  Compliant orders: "+ orderTrackingProcessDtoFilteredList.size());
        return orderTrackingProcessDtoFilteredList;
    }



}
