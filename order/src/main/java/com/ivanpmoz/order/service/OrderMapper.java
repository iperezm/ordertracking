package com.ivanpmoz.order.service;

import java.util.List;

import com.ivanpmoz.order.model.entity.Order;
import com.ivanpmoz.order.service.dto.OrderTrackingProcessDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    @Mappings({
        @Mapping(target="id", source="orderId")
    })
    Order asOrder(OrderTrackingProcessDto orderTrackingProcessDto);
    List<Order> asOrders(List<OrderTrackingProcessDto> orderTrackingProcessDtos);
}
