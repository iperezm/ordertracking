package com.ivanpmoz.order.service.helper.state;

public class EstadoDesconocidoState implements State {

    @Override
    public boolean puedoIrNuevoEstado(State newState) {
        //No aplica
        return false;
    }

    @Override
    public boolean puedeAsignarse() {
        return false;
    }
}
