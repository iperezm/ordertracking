package com.ivanpmoz.order.service.helper.state;

public class EntregadoEstate implements State {

    @Override
    public boolean puedoIrNuevoEstado(State newState) {
        //No puede ir a ningun estado desde entregado
        return false;
    }

}
