package com.ivanpmoz.order.service;

import java.util.List;

import com.ivanpmoz.order.service.dto.OrderTrackingProcessDto;

public interface OrderService {
    public void trackOrders(List<OrderTrackingProcessDto> orderTrackingProcessDtoList);
}
