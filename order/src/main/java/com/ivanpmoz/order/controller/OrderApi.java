package com.ivanpmoz.order.controller;


import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ivanpmoz.order.controller.dto.UpdateOrdersStatusRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Validated
@Api(value = "Order", description = "the Order API")
@RequestMapping(value = "/order")
public interface OrderApi {

    @ApiOperation(value = "Actualiza los estados de los pedidos", nickname = "updateOrdersStatus", notes = "", tags = {
        "Order",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "aInternal error")})
    @RequestMapping(value = "/tracking/",
        consumes = {"application/json", "application/xml"},
        method = RequestMethod.POST)
    ResponseEntity<Void> updateOrdersStatus(
        @ApiParam(value = "", required = true) @Valid @RequestBody UpdateOrdersStatusRequest updateOrdersStatusRequest);
}
