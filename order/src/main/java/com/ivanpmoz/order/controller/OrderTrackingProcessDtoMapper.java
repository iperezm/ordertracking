package com.ivanpmoz.order.controller;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

import com.ivanpmoz.order.controller.dto.OrderTrackingDto;
import com.ivanpmoz.order.service.dto.OrderTrackingProcessDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface OrderTrackingProcessDtoMapper {

    @Mappings({
        @Mapping(target="statusId", source="trackingStatusId")
    })
    OrderTrackingProcessDto asOrderTrackingProcessDto(OrderTrackingDto orderTrackingDto);

    List<OrderTrackingProcessDto> asOrderTrackingProcessDtos(List<OrderTrackingDto> orderTrackingDtos);

    default LocalDateTime map(OffsetDateTime time){
        return time.toLocalDateTime();
    }
}
