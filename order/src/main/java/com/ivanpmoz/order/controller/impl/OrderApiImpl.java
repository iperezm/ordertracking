package com.ivanpmoz.order.controller.impl;


import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.ivanpmoz.order.controller.OrderApi;
import com.ivanpmoz.order.controller.OrderTrackingProcessDtoMapper;
import com.ivanpmoz.order.controller.dto.UpdateOrdersStatusRequest;
import com.ivanpmoz.order.service.OrderService;
import com.ivanpmoz.order.service.dto.OrderTrackingProcessDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class OrderApiImpl implements OrderApi {

    private OrderTrackingProcessDtoMapper orderTrackingProcessDtoMapper;
    private OrderService orderService;

    OrderApiImpl(OrderTrackingProcessDtoMapper orderTrackingProcessDtoMapper, OrderService orderService){
        this.orderTrackingProcessDtoMapper = orderTrackingProcessDtoMapper;
        this.orderService = orderService;
    }

    @Override
    public ResponseEntity<Void> updateOrdersStatus(@Valid UpdateOrdersStatusRequest updateOrdersStatusRequest) {
        if(updateOrdersStatusRequest.getOrderTrackings().isEmpty()){
            log.debug("Bad requuest");
            return ResponseEntity.badRequest().build();
        }
        log.debug("Received ".concat(updateOrdersStatusRequest.getOrderTrackings().toString()) );
        List<OrderTrackingProcessDto> orderTrackingProcessDtos = orderTrackingProcessDtoMapper.asOrderTrackingProcessDtos(updateOrdersStatusRequest.getOrderTrackings());
        log.debug("Mapped: ".concat(orderTrackingProcessDtos.toString()) );
        this.orderService.trackOrders(orderTrackingProcessDtos);
        log.debug("OK ".concat(updateOrdersStatusRequest.toString()) );
        return ResponseEntity.ok().build();
    }
}
