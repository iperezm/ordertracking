package com.ivanpmoz.order.controller.dto;

import java.io.Serializable;
import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class OrderTrackingDto implements Serializable{
        private static final long serialVersionUID = 1L;

        @JsonProperty("orderId")
        private Long orderId;

        @JsonProperty("trackingStatusId")
        private Integer trackingStatusId;

        @JsonProperty("changeStatusDate")
        private OffsetDateTime changeStatusDate;



}
