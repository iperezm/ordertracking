package com.ivanpmoz.order.controller.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UpdateOrdersStatusRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("orderTrackings")
    @Valid
    private List<OrderTrackingDto> orderTrackings = new ArrayList<>();
}
