CREATE DATABASE IF NOT EXISTS orderDB;
CREATE USER 'uorder'@'%' IDENTIFIED BY 'uorderp';
GRANT ALL PRIVILEGES ON orderDB.* to 'uorder'@'%';

CREATE DATABASE IF NOT EXISTS orderAuditlogDB;
CREATE USER 'uorderauditlog'@'%' IDENTIFIED BY 'uorderauditlogp';
GRANT ALL PRIVILEGES ON orderAuditlogDB.* to 'uorderauditlog'@'%';


USE orderDB;
CREATE TABLE IF NOT EXISTS ORDER_TBL  (
    ID BIGINT NOT NULL ,    
    STATUS_ID INT NOT NULL,
    CHANGE_STATUS_DATE TIMESTAMP NOT NULL,
    UPDATE_STATUS_DATE TIMESTAMP NOT NULL,
    PRIMARY KEY ( ID )
);
GRANT SELECT, INSERT, UPDATE ON ORDER_TBL to 'uorder'@'%';

USE orderAuditlogDB;
CREATE TABLE IF NOT EXISTS ORDER_AUDITLOG  (
    ID BIGINT NOT NULL AUTO_INCREMENT,
    ORDER_ID BIGINT NOT NULL,
    STATUS_ID INT NOT NULL,
    CHANGE_STATUS_DATE TIMESTAMP NOT NULL,
    UPDATE_STATUS_DATE TIMESTAMP NOT NULL,
    PRIMARY KEY ( ID )
);
GRANT SELECT, INSERT, UPDATE ON ORDER_AUDITLOG to 'uorderauditlog'@'%';
