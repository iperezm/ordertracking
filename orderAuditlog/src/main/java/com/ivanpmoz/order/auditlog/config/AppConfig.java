package com.ivanpmoz.order.auditlog.config;


import org.springframework.cloud.stream.annotation.EnableBinding;

import com.ivanpmoz.order.auditlog.messaging.BasicOrderAuditlogSink;

@EnableBinding({BasicOrderAuditlogSink.class})
public class AppConfig {
}
