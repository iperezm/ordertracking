package com.ivanpmoz.order.auditlog.service;

import com.ivanpmoz.order.auditlog.model.entity.OrderAuditlog;
import com.ivanpmoz.order.auditlog.service.dto.OrderAuditlogDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderAuditlogMapper {

    OrderAuditlog asOrderAuditlog(OrderAuditlogDto orderAuditlogDto);
}
