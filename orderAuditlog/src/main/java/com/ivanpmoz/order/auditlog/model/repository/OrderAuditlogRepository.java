package com.ivanpmoz.order.auditlog.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ivanpmoz.order.auditlog.model.entity.OrderAuditlog;

@Repository
public interface OrderAuditlogRepository extends JpaRepository<OrderAuditlog, Long> {

}
