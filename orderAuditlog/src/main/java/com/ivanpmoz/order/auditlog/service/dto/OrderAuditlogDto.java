package com.ivanpmoz.order.auditlog.service.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class OrderAuditlogDto {

//    @NotEmpty(message = "El pedido no puede ser vacío")
    private Long orderId;
//    @NotEmpty(message = "El estado no puede ser vacío")
    private Integer statusId;
//    @NotEmpty(message = "La fecha de modificacion de estado no puede ser vacío")
    private LocalDateTime changeStatusDate;
//    @NotEmpty(message = "La fecha de procesado de modificacion de estado no puede ser vacío")
    private LocalDateTime updateStatusDate;


}
