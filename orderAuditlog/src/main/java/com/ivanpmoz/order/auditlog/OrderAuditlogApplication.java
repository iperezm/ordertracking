package com.ivanpmoz.order.auditlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderAuditlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderAuditlogApplication.class, args);
	}

}
