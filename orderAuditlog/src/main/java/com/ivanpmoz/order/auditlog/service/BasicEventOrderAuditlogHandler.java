package com.ivanpmoz.order.auditlog.service;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;


import com.ivanpmoz.order.auditlog.messaging.BasicOrderAuditlogSink;
import com.ivanpmoz.order.auditlog.model.entity.OrderAuditlog;
import com.ivanpmoz.order.auditlog.model.repository.OrderAuditlogRepository;
import com.ivanpmoz.order.auditlog.service.dto.OrderAuditlogDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BasicEventOrderAuditlogHandler {

    private OrderAuditlogRepository orderAuditlogRepository;
    private OrderAuditlogMapper orderAuditlogMapper;

    public BasicEventOrderAuditlogHandler(
        OrderAuditlogRepository orderAuditlogRepository,
        OrderAuditlogMapper orderAuditlogMapper) {
        this.orderAuditlogRepository = orderAuditlogRepository;
        this.orderAuditlogMapper = orderAuditlogMapper;
    }

    @StreamListener(BasicOrderAuditlogSink.INPUT)
    public void orderAuditEventHandle(OrderAuditlogDto orderAuditlogDto) {
        log.debug("Recibido mensaje ".concat(orderAuditlogDto.toString()));
        OrderAuditlog orderAuditlogBBDD = orderAuditlogRepository.save(orderAuditlogMapper.asOrderAuditlog(orderAuditlogDto));
        log.debug("Grabado: ".concat(orderAuditlogBBDD.toString()) );

    }

}

