package com.ivanpmoz.order.auditlog.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BasicOrderAuditlogSink {
    final String INPUT = "sinkInput";

    @Input(INPUT)
    SubscribableChannel input();


}
