package com.ivanpmoz.order.auditlog;



import java.time.LocalDateTime;

import com.ivanpmoz.order.auditlog.model.entity.OrderAuditlog;
import com.ivanpmoz.order.auditlog.model.repository.OrderAuditlogRepository;
import com.ivanpmoz.order.auditlog.service.BasicEventOrderAuditlogHandler;
import com.ivanpmoz.order.auditlog.service.OrderAuditlogMapper;
import com.ivanpmoz.order.auditlog.service.dto.OrderAuditlogDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Slf4j
public class BasicEventOrderAuditlogHandlerTest {

    private BasicEventOrderAuditlogHandler basicEventOrderAuditlogHandler;

    private OrderAuditlogRepository orderAuditlogRepository;
    private OrderAuditlogMapper orderAuditlogMapper;


    BasicEventOrderAuditlogHandlerTest(){
        this.orderAuditlogRepository = Mockito.mock(OrderAuditlogRepository.class);
        this.orderAuditlogMapper = Mockito.mock(OrderAuditlogMapper.class);
        this.basicEventOrderAuditlogHandler = new BasicEventOrderAuditlogHandler(orderAuditlogRepository, orderAuditlogMapper);
    }

    @Test
    public void test() {
        OrderAuditlogDto orderAuditlogDto = OrderAuditlogDto.builder().orderId(1L).statusId(1).changeStatusDate(
            LocalDateTime.now()).updateStatusDate(LocalDateTime.now()).build();
        OrderAuditlog orderAuditlog = OrderAuditlog.builder().orderId(1L).statusId(1).changeStatusDate(
            LocalDateTime.now()).updateStatusDate(LocalDateTime.now()).build();
        when(orderAuditlogMapper.asOrderAuditlog(orderAuditlogDto)).thenReturn(orderAuditlog);
        OrderAuditlog orderAuditlogSaved = OrderAuditlog.builder().orderId(1L).statusId(1).changeStatusDate(
            LocalDateTime.now()).updateStatusDate(LocalDateTime.now()).id(1L).build();
        when(orderAuditlogRepository.save(orderAuditlog)).thenReturn(orderAuditlogSaved);
        this.basicEventOrderAuditlogHandler.orderAuditEventHandle(orderAuditlogDto);
        verify(orderAuditlogMapper, times(1)).asOrderAuditlog(orderAuditlogDto);
        verify(orderAuditlogRepository, times(1)).save(orderAuditlog);
    }



}