# orderTracking

Solución basada en 2 microservicios:
* En uno de ellos guardamos el estado actual del pedido
* En el otro delegamos la escritura del registro basandose en los eventos recibidos por mensajeria (emitidos por el primero)


Para levantar mariadb y rabbitmq:

docker-compose up


Para compilar cada uno de los microservicios y ejecutar pruebas unitarias:

mvn clean package


Para ejecutar cada uno de los microservicios

java -jar microservicio.jar



Posibles mejoras
- API First (tanto en API Rest como en asíncrona)
- Validación
- Gestión de errores: Envíos a cola de error para reproceso, etc
- Test de integracion
- Inclusion microservicios en el docker compose
- Se trata de queries sencillas y acceso por id (como mucho por un índice en el registro). Exploración de uso NOSQL
- Para nota test de contrato






